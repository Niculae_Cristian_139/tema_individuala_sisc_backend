$(document).ready(function () {
	$('select').material_select();
});

document.querySelector('#regbutton').addEventListener(
	'click',
	(e) => {
		e.preventDefault();
		toastr.remove();
		const dateStudent = {
			nume: document.querySelector('#nume').value,
			prenume: document.querySelector('#prenume').value,
			telefon: document.querySelector('#telefon').value,
			email: document.querySelector('#email').value,
			facebook: document.querySelector('#facebook').value,
			data_plecare: document.querySelector('#data_plecare').value,
			data_intoarcere: document.querySelector('#data_intoarcere').value,
			destinatie: document.querySelector('#destinatie').value,
			cnp: document.querySelector('#cnp').value,
			varsta: document.querySelector('#varsta').value,
			cod_bilet: document.querySelector('#cod_legitimatie').value,
		};
		console.log(dateStudent);
		axios
			.post('/student', dateStudent)
			.then((response) => {
				toastr.success('Student adaugat cu succes!');
			})
			.catch((error) => {
				const values = Object.values(error.response.data);
				console.log(error);
				values.map((item) => {
					toastr.error(item);
				});
			});
	},
	false
);
