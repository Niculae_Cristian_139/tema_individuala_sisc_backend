const mysql = require('mysql');
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());
const port = process.env.PORT || 8080;
app.listen(port, () => {
	console.log('Server online on: ' + port);
});
app.use('/', express.static('../front-end'));
const connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'sisc_pasageri',
});
connection.connect(function (err) {
	console.log('Connected to database!');
	const sql =
		"CREATE TABLE IF NOT EXISTS pasageri_inscrisi(nume VARCHAR(255), prenume  VARCHAR(255), telefon  VARCHAR(255) , email  VARCHAR(255),data_plecare VARCHAR(10), data_intoarcere VARCHAR(10), destinatie VARCHAR(255), facebook VARCHAR(255),cod_bilet VARCHAR(255), cnp VARCHAR(20), varsta VARCHAR(3), durata_calatoriei VARCHAR(255), acord_parental BOOLEAN, gen ENUM('M', 'F'))"; //intre paranteze campurile cu datele
	connection.query(sql, function (err, result) {
		if (err) throw err;
	});
});
app.post('/student', async (req, res) => {
	const pasager = {
		nume: req.body.nume,
		prenume: req.body.prenume,
		telefon: req.body.telefon,
		email: req.body.email,
		data_plecare: req.body.data_plecare,
		data_intoarcere: req.body.data_intoarcere,
		destinatie: req.body.destinatie,
		facebook: req.body.facebook,
		cod_bilet: req.body.cod_bilet,
		cnp: req.body.cnp,
		varsta: req.body.varsta,
		durata_calatoriei: null,
		acord_parental: null,
		gen: null,
	};

	let error = [];
	//aici rezolvati cerintele (づ｡◕‿‿◕｡)づ

	Object.entries(pasager).forEach(([key, value]) => {
		if (value === '') {
			error.push(`Campul ${key} nu a fost completat`);
		}
	});

	if (pasager.nume.length < 3 || pasager.nume.length > 30) {
		error.push(
			'Numele pasagerului nu se incadreaza in limita numarului de caractere [3,30]'
		);
	}

	if (pasager.prenume.length < 3 || pasager.prenume.length > 30) {
		error.push(
			'Prenumele pasagerului nu se incadreaza in limita numarului de caractere [3,30]'
		);
	}

	for (let i = 0; i < pasager.nume.length; i++) {
		if (!isNaN(pasager.nume[i])) {
			error.push('Numele trebuie sa contina doar litere');
			break;
		}
	}

	for (let i = 0; i < pasager.prenume.length; i++) {
		if (!isNaN(pasager.prenume[i])) {
			error.push('Prenumele trebuie sa contina doar litere');
			break;
		}
	}

	if (pasager.nume[0] !== pasager.nume[0].toUpperCase()) {
		error.push('Numele trebuie sa inceapa cu litera mare.');
	}

	const varianteAdmise = ['-', ' '];

	let ctrPrenume = 1;
	varianteAdmise.forEach((varianta) => {
		if (pasager.prenume.includes(varianta)) {
			ctrPrenume = 0;
			const prenumeSplit = pasager.prenume.split(varianta);
			prenumeSplit.forEach((element) => {
				if (element[0] !== element[0].toUpperCase) {
					error.push(`Prenumele ${element} trebuie sa inceapa cu litera mare`);
				}
			});
		}
	});

	if (ctrPrenume) {
		const isValid = /^[a-zA-Z]+$/.test(pasager.prenume);
		if (isValid) {
			if (pasager.prenume[0] !== pasager.prenume[0].toUpperCase()) {
				error.push(
					`Prenumele ${pasager.prenume} trebuie sa inceapa cu litera mare`
				);
			}
		} else {
			error.push(
				"Prenumele trebuie sa contina numai litere, spatii sau simbolul '-' "
			);
		}
	}

	if (pasager.telefon.length !== 10) {
		error.push('Numarul de telefon trebuie sa contina 10 cifre');
	}

	for (let i = 0; i < pasager.telefon.length; i++) {
		if (isNaN(pasager.telefon[i])) {
			error.push('Numarul de telefon contine litere');
			break;
		}
	}

	const varianteMail = ['@stud.ase.ro', '@gmail.com', 'yahoo.com'];
	let emailCorect = 0;
	for (let i = 0; i < varianteMail.length; i++) {
		if (pasager.email.includes(varianteMail[i])) {
			emailCorect = 1;
			break;
		}
	}

	if (!emailCorect) {
		error.push('Adresa de mail este gresita');
	}

	const dataPlecare = pasager.data_plecare.split('/');
	const dataIntoarcere = pasager.data_intoarcere.split('/');

	if (dataPlecare.length !== 3) {
		error.push('Structura datei plecarii este gresita');
	} else {
		if (
			(dataPlecare[0][0] === '0' && dataPlecare[0][1] === '0') ||
			(dataPlecare[1][0] === '0' && dataIntoarcere[0][1] === '0') ||
			dataPlecare[2][0] === '0' ||
			dataPlecare.length === 1
		) {
			error.push('Data plecare gresita');
		}
	}

	if (dataIntoarcere.length !== 3) {
		error.push('Structura datei intoarcerii este gresita');
	} else {
		if (
			(dataIntoarcere[0][0] === '0' && dataIntoarcere[0][1] === '0') ||
			(dataIntoarcere[1][0] === '0' && dataIntoarcere[0][1] === '0') ||
			dataIntoarcere[2][0] === '0' ||
			dataIntoarcere.length === 1
		) {
			error.push('Data intoarcere gresita');
		}
	}

	if (dataPlecare.length === 3 && dataIntoarcere.length === 3) {
		if (dataPlecare[2] === dataIntoarcere[2]) {
			if (dataPlecare[1] === dataIntoarcere[1]) {
				if (parseInt(dataPlecare[0]) > parseInt(dataIntoarcere[0])) {
					error.push('Ziua datei plecarii este gresita');
				}
			} else {
				if (parseInt(dataPlecare[1]) > parseInt(dataIntoarcere[1])) {
					error.push('Luna datei plecarii este gresita');
				}
			}
		} else {
			if (parseInt(dataPlecare[2]) > parseInt(dataIntoarcere[2])) {
				error.push('Anul datei plecarii este gresit');
			}
		}
	}

	let ctrDestinatie = 1;
	varianteAdmise.forEach((varianta) => {
		if (pasager.destinatie.includes(varianta)) {
			ctrDestinatie = 0;
			const destinatieSplit = pasager.destinatie.split(varianta);
			destinatieSplit.forEach((element) => {
				if (element[0] !== element[0].toUpperCase) {
					error.push(`Destinatia ${element} trebuie sa inceapa cu litera mare`);
				}
			});
		}
	});

	if (ctrDestinatie) {
		const isValid = /^[a-zA-Z]+$/.test(pasager.destinatie);
		if (isValid) {
			if (pasager.destinatie[0] !== pasager.destinatie[0].toUpperCase()) {
				error.push(
					`Destinatia ${pasager.destinatie} trebuie sa inceapa cu litera mare`
				);
			}
		} else {
			error.push(
				"Destinatia trebuie sa contina numai litere, spatii sau simbolul '-' "
			);
		}
	}

	if (!pasager.facebook.includes('www.facebook.com')) {
		error.push('Formatul link-ului de facebook nu este corect');
	}

	const codBilet = pasager.cod_bilet.split('IR');
	if (codBilet.length === 1) {
		error.push('Structura codului bilet nu este corecta');
	} else {
		if (codBilet[1].length !== 6) {
			error.push('Codul trebuie sa contina 6 cifre');
		} else {
			if (!/^\d+$/.test(codBilet[1])) {
				error.push('Codul trebuie sa contina numai cifre');
			}
		}
	}

	if (!/^\d+$/.test(pasager.cnp) || pasager.cnp.length !== 13) {
		error.push('CNP gresit');
	}

	if (pasager.varsta.length < 1 || pasager.varsta.length > 3) {
		error.push('Varsta gresita');
	}
	// 0 12 34 56 78 9 10 11 12
	// 6 12 04 20 125555
	// S AA LL ZZ
	if (pasager.cnp.length === 13) {
		const today = new Date();
		const yyyy = today.getFullYear();
		let mm = today.getMonth() + 1;
		let dd = today.getDate();

		if (dd < 10) dd = '0' + dd;
		if (mm < 10) mm = '0' + mm;

		const formattedToday = dd + '/' + mm + '/' + yyyy;
		let data_nastere;
		if (pasager.cnp[0] === '5' || pasager.cnp[0] === '6') {
			data_nastere = `${pasager.cnp[5]}${pasager.cnp[6]}/${pasager.cnp[3]}${
				pasager.cnp[4]
			}/${100 + parseInt('19' + pasager.cnp[1] + pasager.cnp[2])}`;
		} else {
			data_nastere = `${pasager.cnp[5]}${pasager.cnp[6]}/${pasager.cnp[3]}${
				pasager.cnp[4]
			}/${'19' + pasager.cnp[1] + pasager.cnp[2]}`;
		}
		const t = new Date(
			formattedToday.split('/')[2],
			formattedToday.split('/')[1] - 1,
			formattedToday.split('/')[0]
		);
		const d2 = new Date(
			data_nastere.split('/')[2],
			data_nastere.split('/')[1] - 1,
			data_nastere.split('/')[0]
		);

		const diffTime = Math.abs(t.getTime() - d2.getTime());
		const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
		const varstaInAni = Math.floor(diffDays / 365);
		if (varstaInAni !== parseInt(pasager.varsta)) {
			error.push('Varsta nu corespunde datei din CNP');
		} else {
			pasager.varsta = varstaInAni;
		}
	}

	const durataPlecare = new Date(
		pasager.data_plecare.split('/')[2],
		pasager.data_plecare.split('/')[1] - 1,
		pasager.data_plecare.split('/')[0]
	);
	const durataIntoarcere = new Date(
		pasager.data_intoarcere.split('/')[2],
		pasager.data_intoarcere.split('/')[1] - 1,
		pasager.data_intoarcere.split('/')[0]
	);

	const diffTime = Math.abs(
		durataIntoarcere.getTime() - durataPlecare.getTime()
	);
	const durataInZile = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
	if (durataInZile > 14) {
		error.push('Durata calatoriei mai mare decat 14');
	} else {
		pasager.durata_calatoriei = durataInZile;
	}

	pasager.acord_parental = !(pasager.varsta > 18);

	if (
		pasager.cnp[0] === '1' ||
		pasager.cnp[0] === '3' ||
		pasager.cnp[0] === '5'
	) {
		pasager.gen = 'M';
	} else {
		pasager.gen = 'F';
	}

	const sql = `SELECT * FROM pasageri_inscrisi WHERE email = ?`;

	const query = new Promise((resolve, reject) => {
		connection.query(sql, [pasager.email], (err, result) => {
			if (err) reject(err);
			if (Object.values(JSON.parse(JSON.stringify(result))).length > 0) {
				reject('Email deja inregistrat');
			} else {
				resolve('Succes');
			}
		});
	});

	try {
		await query;
	} catch (err) {
		error.push(err);
	}

	if (error.length === 0) {
		const sql = `INSERT INTO pasageri_inscrisi (
      nume,
      prenume,
      telefon,
      email,
      data_plecare,
      data_intoarcere,
      destinatie,
      facebook,
      cod_bilet,
      cnp,
      varsta, 
      durata_calatoriei,
      acord_parental,
      gen) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
		connection.query(
			sql,
			[
				pasager.nume,
				pasager.prenume,
				pasager.telefon,
				pasager.email,
				pasager.data_plecare,
				pasager.data_intoarcere,
				pasager.destinatie,
				pasager.facebook,
				pasager.cod_bilet,
				pasager.cnp,
				pasager.varsta,
				pasager.durata_calatoriei,
				pasager.acord_parental,
				pasager.gen,
			],
			function (err, result) {
				if (err) throw err;
				console.log('pasager creat cu succes!');
				res.status(200).send({
					message: 'pasager creat cu succes',
				});
				console.log(sql);
			}
		);
	} else {
		res.status(500).send(error);
		console.log('pasagerul nu a putut fi creat!');
	}
	app.use('/', express.static('../front-end'));
});
//modifica si din front la index.html
